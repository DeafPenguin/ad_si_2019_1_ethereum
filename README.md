Introdução e Minitutorial de Ethereum
===

* Disciplina: Sistemas de Informação
* Ano letivo: 2019-1
* Professor: M.Prof.º Marcelo Akira Inuzuka
* Aluno: Victor Murilo Balbino Machado
* Matrícula 20122414

# Conceitos e Definições
## O que é Ethereum?

É considerado, por uma considerável parcela de pessoas, como uma evolução da blockchain. É uma plataforma, baseada no blockchain, que programa aplicativos descentralizados (dApps), além de contratos inteligentes e transações da criptomoeda Ether. Foi idealizada por **Vitalik Buterin**, em 2013, porém ficando online apenas em meados de julho de 2015.
Ele possui uma máquina virtual descentralizada Turing completude, a Ethereum Virtual Machine (EVM), que pode executar scripts usando uma rede internacional de nós públicos.
O Ethereum visa levar a tecnologia do blockchain e os contratos inteligentes para, literalmente, tudo que possa ser programado.

## E Geth? O que seria?

O Geth é a implementação em GoLang (https://golang.org/) do ethereum. Este software possibilita que seu computador seja um nó da rede e assim podendo participar de uma rede Blockchain Ethereum, criar soluções distribuidas e descentralizadas, criar contratos e dentre outros. [via https://imasters.com.br/devsecops/o-que-e-ethereum]

## Aplicações Descentralizadas

Apesar do Ethereum ser uma plataforma baseada no blockchain e ser muito parecido com o bitcoin, o foco principal é a programação aplicações descentralizadas e contratos inteligentes. Segue alguns exemplos de aplicações descentralizadas:

* Augur, uma rede de apostas sem um dono (https://www.augur.net/)
* Maker Dao/Dai, que é uma moeda pareada com o dólar (https://makerdao.com/en/)
* Golem, uma proposta baseada no ethereum em que pode gerar capital através do uso de redes sociais (https://golem.network/)
* Comparativo de alguns dApps (Fonte: https://cointimes.com.br/avaliacao-de-criptomoedas-ethereum/):

![Alt Text](https://i.imgur.com/5lcHSjX.png)

A principal diferença entre Aplicativos comuns e dAPPs, é que os comuns são uma interface entre os programas com algum código salvo na nuvem, e os dAPPs são uma interface para algum código descentralizado salvo na Blockchain.
Sendo assim, DAPPs não possuem uma autoridade central para funcionar e depende somente de sua rede.
As maiores vantagens para os dApps é que os mesmo são geralmente de opensource de forma que possam ser auditados sua segurança, geram tokens para mineradores e para quem transaciona na rede também.

![Alt Text](https://livecoins.com.br/wp-content/uploads/2018/03/1_L5ApbOvu0Pf-oRXFYd3vkA-360x250.png)

## Contratos Inteligentes - SmartContracts
Os SmartContracts são contratos de assinatura digital cuja verificação é realizada por meio de protocolos de computador, evitando toda possibilidade de tempo de inatividade, censura, fraude ou manipulação por terceiros, garantindo o desempenho de transações confiáveis sem terceiros. Na rede Ethereum, são implementados em quatro linguagens diferentes, sendo compilados para a máquina virtual do Ethereum e em seguida anexados à blockchain personalizado.

As linguagens são:
* Solidity (JavaScript)
* Serpent (Python)
* Mutan (C)
* LLL (Lisp)

![Alt Text](https://i.imgur.com/pCZEWrF.png)

Exemplos de uso: Serviços financeiros, processos legais, contratos de violação, acordos de crowdfunding, lei de propriedade, cumprimento de crédito e etc.

# Instalação e Uso

## Executável (Binária)

Todas versões de Geth estão disponíveis em https://geth.ethereum.org/downloads/. O download já vem com o instalador dentro do arquivo zip, do qual já configura o PATH.

1. Baixe o arquivo .zip
2. Extraia o executável
3. Abra o prompt de comando
4. Digite e rode o comando "chdir"
5. Abra o geth.exe

## Criando um blockchain e minerando Ether[⁷]

Com o Geth instalado, precisamos do arquivo Genesis (representa o primeiro bloco do blockchain). Crie um arquivo chamado genesis.json com o conteúdo abaixo:

```sh
{
    "difficulty": "400",
    "gasLimit": "2100000",
    "alloc": {
    },
    "config": {
            "chainId":        15,
            "homesteadBlock": 0,
            "eip155Block":    0,
            "eip158Block":    0
    }
}
```

Agora definiremos o diretório onde criaremos nosso blockchain, e assim criar ele (3ª linha).

```sh
# Defina o diretório que você irá utilizar para criar seu blockchain
$ export PRIVATE_CHAIN_DIR=/home/someone/private-chain-01
$ geth --datadir $PRIVATE_CHAIN_DIR init genesis.json
```

Agora podemos nos conectar ao blockchain criado, através do seguinte comando:

```sh
$ geth --datadir $PRIVATE_CHAIN_DIR --networkid 15 --rpc --rpc --rpcapi "db,eth,net,web3" --rpcport "8545" console
```

Criando uma conta pessoal (Esse comando cria uma conta recebendo uma senha definida por você como parâmetro):

```sh
> personal.newAccount("senha")
```

Vai gerar uma saída em que exibira o endereço da conta criada, da qual você pode usar para poder mandar o ether minerado:

```sh
> miner.setEtherbase("**endereço gerado aqui**")
> miner.start(3)
```

E para finalizar a mineração, basta usar o comando a seguir:

```sh
> miner.stop()
```

## Criando um SmartContract[⁸]

Para criar este contrato, utilizaremos a linguagem Solidity (baseada no Javascript, como dito acima), e será mostrado todo o seu ciclo de vida. Primeiramente escolhemos um diretório e criamos um arquivo com a extensão .sol (para testar este código, criei um arquivo chamado Teste.sol):

```sh
pragma solidity ^0.4.23;

contract Mortal {
    address victor;
    constructor() public {victor = msg.sender;}
    function apagar() public {
        if (msg.sender == victor) {selfdestruct(victor)}
    }
}
contract HelloWorld is Mortal {
    string mensagem;
    constructor(string _mensagem) public {mensagem = _mensagem;}
    function sayHello() public constant returns (string) {return mensagem;}
}
```

Assim que terminar, salve o arquivo. Agora é necessário publicar o contrato para que seja efetivado:

```sh
$ echo "var HelloWorldCompilado=`solc --optimize --combined-json abi,bin,interface Teste.sol`" > compilado.js
```

O comando acima faz com que o arquivo .sol seja compulado, retornando um JSON, e concatenando o mesmo à uma declaração de uma variável em JavaScript e salva em um arquivo chamado compilado.js.

Agora precisamos abrir o console do Geth:

```sh
$ geth --rinkeby --verbosity 0 console --mine
```

E neste console, carregamos o arquivo gerado:

```sh
> loadScript('compilado.js')
```
Vamos guardar a Application Binery Interface (ABI) numa variável e também o código binário do smartcontract:

```sh
> var abi = JSON.parse(HelloWorldCompilado.contracts['Teste.sol:HelloWorld'].abi);
> var bin = '0x'+HelloWorldCompilado.contracts['Teste.sol:HelloWorld'].bin;
```

Precisamos de uma instância de um objeto de contrato no JavaScript:

```sh
> var contrato = eth.contract(abi);
```

## Publicando o contrato!

Agora vou abordar sobre a publicação do contrato, lembrando que para publicar **é necessário possuir eth** para pagar aos mineradores pela publicação. Assim que você conseguir eth (existe diversas maneiras), precisamos liberar a conta criada para pagamento:

```sh
> personal.unlockAccounts(eth.accounts[0]);
```

Evocamos o método new, passando como parâmetro: a mensagem que será guardada, a conta, o binário, a quantidade de gas que vai ser paga, e uma função que será executada em caso de modificação na publicação:

```sh
> var meuContrato = contrato.new('Olá mundo dos contratos!',{ from: eth.accounts[0], data: bin, gas: 4700000},
  function (e, contract) {
    if (typeof contract.address !== 'undefined') {
         console.log('Contrato publicado! Endereço: ' + contract.address + ' transactionHash: ' + contract.transactionHash);
    }
  }
);
```

Vai ser retornado uma mensagem dizendo que o contrato foi publicado e um endereço.

Você pode interagir com o contrato localmente, por meio do console:

```sh
> meuContrato.sayHello();
"Olá mundo dos contratos!"
```

Smartcontracts são eternos por padrão, mas podemos programar para que esse contrato seja inutilizado:

```sh
> meuContrato.apagar.sendTransaction({from:eth.accounts[0]})
```

Após alguns instantes a transação será efetuada, então podemos verificar, o que deve retornar um "0x":

```sh
> eth.getCode(meuContrato.address)
"0x"
```

## Comentários do autor (Victor Murilo)

O Ethereum pegou o embalo da idéia de blockchain e enxergou um horizonte mais longe do que quando foi idealizada por Satoshi, o criador do bitcoin, da qual projetou o mesmo para transações financeiras. A ideia de firmar contrato para diversos tipos de atividades é algo muito mais abrangente e interessante, principalmente no ponto de vista jurídico e financeiro, visto que diminui tempo gasto e é mais seguro, quase podendo ser chamado de imaculado.
Outro ponto forte da tecnologia foi os dApps, que é uma nova forma, talvez melhor, de criação de aplicações, das quais estas são descentralizadas.

Já se tratando de algumas possibilidades, vi oportunidade para se trabalhar no ramo de linguística, visto que um dos maiores problemas na LIBRAS (Lingua Brasileira de Sinais) é a descentralização, gerando diversos sinais diferentes para o mesmo significado, ou seja, uma regionalidade linguística.
Se fosse de comum acordo entre as entidades representantes em diversos munícipios, poderiam cooperar e utilizar dos smartcontracts para o registro de cada informação, seja antiga ou recém descoberta, de forma a unificar e melhorar a gramática da lingua de sinais.

Outra possibilidade futura seria a criação de uma legenda automatizada através dos dApps, que poderia ser otimizado na questão de armazenamento de uma base de áudio, visto que é extremamente caro por conta de ser uma quantidade absurda de dados.

Em resumo, são tecnologias que ainda tem bastante fruto para ser colhido, pode ser adaptado para diversas necessidades e ocasiões e trazendo benefícios enormes para toda a sociedade.

# Referências

1.  https://foxbit.com.br/ - **Usado para definições.**
2.  https://gitlab.com/gabrielamansur00/projeto-4 - **Usado como modelo de construção de trabalho e comparativo.**
3.  https://github.com/ethereum/go-ethereum/wiki/Installation-instructions-for-Windows - **Guia de instalação para Windows.**
4.  https://geth.ethereum.org/downloads/ - **Página de download de arquivos binários.**
5.  https://applicature.com/blog/blockchain-technology/blockchain-development-tutorial - **Material de estudo**
6.  https://bitcoinmagazine.com/articles/ethereum-next-generation-cryptocurrency-decentralized-application-platform-1390528211 - **Buterin, Vitalik (23 de janeiro de 2014) - Bitcoin Magazine.**
7.  https://medium.com/@cristianoandrade/criando-um-blockchain-ethereum-para-testes-utilizando-o-geth-8ec9191ca493 - **Guia de criação de blockchain de ethereum**
8.  https://github.com/ethereum/go-ethereum/wiki/Contract-Tutorial - **Guia de criação de smart contracts**
9.  https://livecoins.com.br/o-que-sao-dapps-e-qual-sua-importancia/ - **dApps e definições**